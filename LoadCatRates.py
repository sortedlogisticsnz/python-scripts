import http.client, json
# Build Connection
conn = http.client.HTTPSConnection("warehouse.sortedlogistics.co.nz")
connEnv = "Live"
headers = {'Content-Type': 'application/json'}
username = "BPALKAPI"
password = "DNHBfnerZ6NeRWFAJu6eQ"
token = ""

clientOid = 6584.1917 #Make sure this is set to the correct client

oidStr = str(clientOid)

# What are the charge codes oids to load?
drum = json.loads("{\"code\":\"810D\", \"oid\": \"9383.5017\"}")
pail = json.loads("{\"code\":\"810P\", \"oid\": \"9383.5020\"}")
carton = json.loads("{\"code\":\"810C\", \"oid\": \"9383.5019\"}")
unit = json.loads("{\"code\":\"810U\", \"oid\": \"9383.5045\"}")

def doApiCall(payload):
    conn.request("POST", "/" + connEnv + "RestfulJsonServiceWH/jadehttp.dll?RestfulJsonServiceWH=null", payload, headers)
    res = conn.getresponse()
    response = json.loads(res.read())
    clientCats = ""
    if response['errorCode'] == 0 :
        return response
    else:
        return "ERROR - " + response['errorMessage']

# Get Token
print("INFO - Logging In")
loginPayload = "{\"command\":\"ValidateUser\",\"parameters\":{\"userid\":\"" + username + "\",\"password\":\"" + password + "\",\"class\":\"WHwebUser\"}}"
logResponse = doApiCall(loginPayload)
if logResponse['errorCode'] == 0 :
    logdata = logResponse['data']
    token = logdata['token'] 
    print("INFO - Token:" + token + " issued")   
else:
    print (logResponse['errorMessage'])
    exit

#Get Client Cats
print("INFO - Getting Categorys for oid " + oidStr)
catPayload = "{\"command\": \"GetClassObjects\",\"parameters\": {\"class\": \"WHcategoryPrimary\",\"token\": \"" + token + "\",\"filter\": {\"rWHclient\":" + oidStr + "},\"properties\": {\"catNo\": \"\",\"description\": \"\",\"units\": \"\",\"unitsOnHand\": \"\",\"kgPerUnit\": \"\"}}}"
catResponse = doApiCall(catPayload)
clientCats = ""
if catResponse['errorCode'] == 0 :
    data = catResponse['data']
    clientCats = data['objects']
else:
    print ("ERROR - " + catResponse['errorMessage'])
    exit

#Loop The List
for cat in clientCats:
    #Update the Charge
    print ("INFO - Working on Cat: " + cat['catNo'] + " - Description: " + cat['description'] + " - Unit: " + cat['units'])
    if cat['units'] == "DR" :
        print("INFO - Cat " + cat['catNo'] + " is a Drum. Loading " + drum['code'] )
        drumPayload = "{\"command\":\"UpdateObject\",\"parameters\":{\"token\":\"" + token + "\",\"oid\":\"" + str(cat['oid']) + "\",\"edition\":" + str(cat['edition']) + ",\"rWHclientCostCodeDespatch1\":\"" + str(drum['oid']) +"\"}}"
        drumupdate = doApiCall(drumPayload)
        if drumupdate['errorCode'] == 0 :
            print ("INFO - Cat " + cat['catNo'] + " succesfully loaded with " + drum['code'])
        else:
            print ("ERROR - " + drumupdate['errorMessage'])

    elif cat['units'] == "PA" :
        print("INFO - Cat " + cat['catNo'] + " is a Pail. Loading " + pail['code'] )
        pailPayload = "{\"command\":\"UpdateObject\",\"parameters\":{\"token\":\"" + token + "\",\"oid\":\"" + str(cat['oid']) + "\",\"edition\":" + str(cat['edition']) + ",\"rWHclientCostCodeDespatch1\":\"" + str(pail['oid']) +"\"}}"
        pailupdate = doApiCall(pailPayload)
        if pailupdate['errorCode'] == 0 :
            print ("INFO - Cat " + cat['catNo'] + " succesfully loaded with " + drum['code'])
        else:
            print ("ERROR - " + pailupdate['errorMessage'])

    elif cat['units'] == "EA" :
        print("INFO - Cat " + cat['catNo'] + " is a Unit. Loading " + unit['code'] )
        unitPayload = "{\"command\":\"UpdateObject\",\"parameters\":{\"token\":\"" + token + "\",\"oid\":\"" + str(cat['oid']) + "\",\"edition\":" + str(cat['edition']) + ",\"rWHclientCostCodeDespatch1\":\"" + str(unit['oid']) +"\"}}"
        unitupdate = doApiCall(unitPayload)
        if unitupdate['errorCode'] == 0 :
            print ("INFO - Cat " + cat['catNo'] + " succesfully loaded with " + unit['code'])
        else:
            print ("ERROR - " + unitupdate['errorMessage'])

    elif cat['units'] == "CT" :
        print("INFO - Cat " + cat['catNo'] + " is a Carton. Loading " + carton['code'] )
        cartonPayload = "{\"command\":\"UpdateObject\",\"parameters\":{\"token\":\"" + token + "\",\"oid\":\"" + str(cat['oid']) + "\",\"edition\":" + str(cat['edition']) + ",\"rWHclientCostCodeDespatch1\":\"" + str(carton['oid']) +"\"}}"
        cartonupdate = doApiCall(cartonPayload)
        if cartonupdate['errorCode'] == 0 :
            print ("INFO - Cat " + cat['catNo'] + " succesfully loaded with " + drum['code'])
        else:
            print ("ERROR - " + cartonupdate['errorMessage'])     
    else :    
        print("ERROR - No matching charge type found for Cat " + cat['catNo'])

#Close the Web Connection
conn.close()

